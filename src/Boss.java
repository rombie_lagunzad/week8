/*

Rombie Lagunzad
CIT 360
Brother Lawrence

This class extends the Thread class by creating a thread for boss.

 */

public class Boss extends Thread {

    private String boss;
    private int hp;
    private int attack;

    public Boss(String boss, int hp, int attack) {
        this.boss = boss;
        this.hp = hp;
        this.attack = attack;
    }
    
    public int getAttack() {
        return attack;
    }

    public void run() {

        try {
            System.out.println("Boss alert: "+ boss + " appears");
            System.out.println("Health: " + hp);
            System.out.println("Attack: " + attack);
            System.out.println("The dragon is in burst mode and keeps attacking");

            for (int count = 1; hp > 0; count++) {
                hp = hp - 400;
                System.out.println("\n" + boss + "\nRemaining Health: " + hp);
            }

            if (hp <= 0) {
                System.out.println("\nThe " + boss + " has been slain.");
                System.out.println("\nVictory!");
                System.exit(0);
            }
        }

        catch (Exception e) {
           System.out.println("An error occurred - Please restart");
       }
    }
}