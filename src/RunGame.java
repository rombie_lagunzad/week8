/*

Rombie Lagunzad
CIT 360
Brother Lawrence

This class implements the Runnable interface. I also included an atomic variable to proceed increment
even in multiple threads.

 */

import java.util.Random;
import java.util.concurrent.atomic.*;

public class RunGame implements Runnable {

    private String character;
    private int hp;
    private int pause;
    private int enemyAttack;
    private AtomicInteger attacks = new AtomicInteger(0);


    public RunGame(String character, int hp, int enemyAttack) {
        this.character = character;
        this.hp = hp;
        this.enemyAttack = enemyAttack;

        Random time = new Random();
        this.pause = time.nextInt(100);
    }

    public void run() {

        System.out.println("\n" + character + "\nHealth: " + hp);

        for (int count = 1; hp > 0; count++) {
            System.out.println("\nThe " + character + " is still alive");
            hp = hp - enemyAttack;
            attacks.incrementAndGet();
            System.out.println("\n" + character + "\nRemaining Health: " + hp);
            System.out.println("Total number of successful opponent's attacks: "+ attacks);
        }

        if (hp <= 0) {
            try {
                // Causes the currently executing thread to sleep
                Thread.sleep(pause);
                System.out.println("\nThe " + character + " has been slain.");
            }
            catch (InterruptedException e) {
                System.out.println("Error occurred: The thread is interrupted");
            }
        }
    }
}


