/*

Rombie Lagunzad
CIT 360
Brother Lawrence

This class is used to execute the runnable task and thread from the RunGame class and Boss class.

 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Players {

    public static void main(String[] args) {


        Boss boss1 = new Boss("Desert Dragon",10000, 100);
        boss1.start();

        ExecutorService startBattle = Executors.newFixedThreadPool(2);

        RunGame player1 = new RunGame("Sorcerer",500, boss1.getAttack());
        RunGame player2 = new RunGame("Warrior",900, boss1.getAttack());
        RunGame player3 = new RunGame("Slayer",800, boss1.getAttack());
        RunGame player4 = new RunGame("Cleric",900, boss1.getAttack());
        RunGame player5 = new RunGame("Cleric",900, boss1.getAttack());

        Thread backupPlayer1 = new Thread(player4);
        Thread backupPlayer2 = new Thread(player5);

        startBattle.execute(player1);
        startBattle.execute(player2);
        startBattle.execute(player3);

        backupPlayer1.start();
        backupPlayer2.start();

        startBattle.shutdown();

    }

}
